package sk.ite.got.gateway.castle.application.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.ite.got.gateway.castle.application.dto.DTOCastle;

import java.util.List;

/**
 * Castle backend service client implemented using Netflix Feign client library.
 *
 */

@FeignClient("castle")
public interface CastleReader {
	@RequestMapping(method = RequestMethod.GET, value = "/castle")
    List<DTOCastle> getAllCastles();

}



