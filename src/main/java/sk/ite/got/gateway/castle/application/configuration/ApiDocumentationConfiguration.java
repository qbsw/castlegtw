package sk.ite.got.gateway.castle.application.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class ApiDocumentationConfiguration {
	
	@Bean
	public Docket castleApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
					.apis(RequestHandlerSelectors.any())
					.paths(PathSelectors.regex("/castle.*"))
					.build()
				.enableUrlTemplating(true)
				.apiInfo(castleApiInfo())
				.tags(new Tag("Castle endpoint", "Use to retrieve castles using various criteria, or to manage them."));
	}
	
	private ApiInfo castleApiInfo() {
		return new ApiInfoBuilder()
                .title("Castle service API")
                .description("API for Castle catalogue service.")
                .build();
	}

}
