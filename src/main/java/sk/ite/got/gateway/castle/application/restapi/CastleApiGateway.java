package sk.ite.got.gateway.castle.application.restapi;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.got.gateway.castle.application.dto.DTOCastle;
import sk.ite.got.gateway.castle.application.service.CastleReader;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Castle API endpoint")
@RestController
@RequestMapping("/castle")
class CastleApiGateway {

	@Autowired
	private CastleReader parkingReader;

	public List<DTOCastle> fallback() {
		List<DTOCastle> fb = new ArrayList<DTOCastle>();
		fb.add(new DTOCastle());
		return fb;
	}

	@HystrixCommand(fallbackMethod = "fallback")
	@ApiOperation(value = "Get all castles", notes = "Retrieves all castles from catalogue.")
	@RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<DTOCastle> getAllCastles() {
		return (List<DTOCastle>) parkingReader.getAllCastles();
	}
}
